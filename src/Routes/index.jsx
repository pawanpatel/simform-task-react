import React, { useEffect } from 'react'
import { Switch, Route } from "react-router-dom";
import { Layout } from 'antd';
import HeaderComponent from '../Components/HeaderComponent';
import FormListComponent from '../Components/FormListComponent';
import CreateFormComponent from '../Components/CreateFormComponent';
import DisplayFormContainer from '../Containers/DisplayFormContainer';
import ReduxHOC from '../hoc/ReduxHOC';
const { Header, Content } = Layout;

const Routes = ({ actions }) => {

    useEffect(() => {
        actions.getAllForms()
    }, [])

    return (
        <div className="routes-main-container">
            <Switch>
                <Route exact path="/form/:formId">
                    <DisplayFormContainer />
                </Route>
                <Route path="/">
                    <Layout className="layout">
                        <Header>
                            <HeaderComponent />
                        </Header>
                        <Content style={{ padding: '20px 50px' }}>
                            <Switch>
                                <Route exact path="/create-new-form">
                                    <CreateFormComponent />
                                </Route>
                                <Route path="/">
                                    <FormListComponent />
                                </Route>
                            </Switch>
                        </Content>
                    </Layout>
                </Route>
            </Switch>
        </div>
    )
}

export default ReduxHOC(Routes)
