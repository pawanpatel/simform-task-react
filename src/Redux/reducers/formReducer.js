const initialState = []

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_FORMS_DATA':
            return [...action.payLoad || []]
        default:
            return [...state]
    }
}