import Axios from 'axios'
import baseUrl from '../constants/baseUrlConfig'

const makeRequest = {
    get: (url) => {
        return new Promise((resolve, reject) => {
            Axios.get(baseUrl + url)
                .then(response => {
                    resolve(response)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    post: (url, data) => {
        return new Promise((resolve, reject) => {
            Axios({
                method: 'post',
                url: baseUrl + url,
                data
            })
                .then(response => {
                    resolve(response)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
}

export default makeRequest