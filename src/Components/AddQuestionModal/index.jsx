import React from 'react'
import { Modal, Input, Form, Select } from 'antd';

const AddQuestionModal = ({ addFormItem, onCancel }) => {
    const [form] = Form.useForm();
    const onFinish = (values) => {
        let data = { ...values }
        if (values?.options) {
            data.options = values.options.split("\n")
        }
        addFormItem({ ...data })
    }

    const submitForm = () => {
        form.validateFields()
            .then(values => {
                onFinish(values)
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div className="add-question-modal-conteiner">
            <Modal
                title="Add New Question"
                visible={true}
                onOk={() => { }}
                okButtonProps={{ onClick: submitForm }}
                onCancel={onCancel}
            >
                <Form
                    layout="vertical"
                    form={form}
                    name="add--question"
                    onFinish={onFinish}
                >
                    <Form.Item label="Question / Title" name="question" rules={[{ required: true, message: "Please enter question/title" }]}>
                        <Input placeholder="Enter question/title" />
                    </Form.Item>
                    <Form.Item shouldUpdate label="Answer type" name={"answerType"} rules={[{ required: true, message: "Please select answer type" }]} >
                        <Select placeholder="Select answer type">
                            <Select.Option value="text">Text</Select.Option>
                            <Select.Option value="multichoice">Multichoice</Select.Option>
                            <Select.Option value="checkbox">Checkbox</Select.Option>
                            <Select.Option value="singleSelect">Single Select</Select.Option>
                            <Select.Option value="radio">Radio</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item shouldUpdate>
                        {() => {
                            return (
                                ["multichoice", "checkbox", "singleSelect", "radio"]?.includes(form.getFieldValue('answerType')) ?
                                    <Form.Item name={"options"} label="Choices" rules={[{ required: true, message: "Please enter choices" }]}>
                                        <Input.TextArea rows={4} placeholder="Enter choices in seprate line" />
                                    </Form.Item>
                                    : null
                            )
                        }}
                    </Form.Item>

                </Form>

            </Modal>
        </div>
    )
}

export default AddQuestionModal
