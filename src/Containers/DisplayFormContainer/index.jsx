import { message, Spin, Layout, Result } from 'antd'
import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router'
import FormComponent from '../../Components/FormComponent'
import makeRequest from '../../utils/makeRequest'

const { Content } = Layout

const DisplayFormContainer = ({ match }) => {

    const [formData, setFormData] = useState({ data: {}, status: "loading" })

    useEffect(() => {
        if (match?.params?.formId) {
            makeRequest.get(`form/${match.params.formId}`)
                .then(response => {
                    if (response?.data?.status?.code == 200) {
                        setFormData({ data: response.data?.data || {}, status: "success" })
                    }
                })
                .catch(err => {
                    setFormData({ data: {}, status: "error" })
                    // message.error("Something went Wrong. Please try after sometime")
                })
        }
    }, [])

    return (
        <div className="display-form-mainContainer">
            <Layout className="layout">
                <Content style={{ padding: '20px 50px' }}>
                    {
                        formData?.status == "success" ?
                            <FormComponent formData={formData.data} />
                            :
                            formData?.status == "error" ?
                                <Result
                                    status="404"
                                    title="404"
                                    subTitle="Sorry, the page you visited does not exist."
                                />
                                : <Spin spinning="true" />
                    }
                </Content>
            </Layout>
        </div>
    )
}

export default withRouter(DisplayFormContainer)
