import React from 'react'
import { Radio, Input, Form, Select, Button, Checkbox } from 'antd';

const FormComponent = ({ formData }) => {

    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log(values)
    }

    return (
        <div className="form-component-container">
            <h3 style={{ textAlign: 'center' }}>{formData?.name}</h3>
            <Form
                layout="vertical"
                form={form}
                name="sample-form"
                onFinish={onFinish}
            >
                {
                    formData?.configuration?.map((item, index) => {
                        return <Form.Item key={index} label={item.question} >
                            {
                                item?.answerType == "text" ?
                                    <Input />
                                    : item?.answerType == "radio" ?
                                        <Radio.Group>
                                            {
                                                item.options.map((option, i) => <Radio key={i} value={option}>{option}</Radio>)
                                            }
                                        </Radio.Group>
                                        : item?.answerType == "checkbox" ?
                                            <Checkbox.Group>
                                                {
                                                    item.options.map((option, i) => <Checkbox key={i} value={option}>{option}</Checkbox>)
                                                }
                                            </Checkbox.Group>
                                            :
                                            <Select mode={item?.answerType == "multichoice" ? "multiple" : null}>
                                                {
                                                    item.options.map((option, i) => <Select.Option key={i} value={option}>{option}</Select.Option>)
                                                }
                                            </Select>
                            }

                        </Form.Item>
                    })
                }
                {
                    formData?.name ?
                        <Button tupe="primary" htmlType="submit"> Submit</Button>
                        : null
                }
            </Form>
        </div>
    )
}

export default FormComponent
