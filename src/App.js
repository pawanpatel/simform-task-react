import Routes from './Routes';
import 'antd/dist/antd.css';
import './scss/core.scss'
import store from './Redux'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
