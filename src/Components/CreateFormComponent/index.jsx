import React, { useState } from 'react'
import { Form, Input, Button, Modal } from 'antd';
import FormComponent from '../FormComponent';
import AddQuestionModal from '../AddQuestionModal';
import makeRequest from '../../utils/makeRequest'
import { CREATE_NEW_FORM } from '../../constants/urlConstants';
import { withRouter } from 'react-router';
import reduxHOC from '../../hoc/ReduxHOC';

const CreateFormComponent = ({ history,actions }) => {
    const [formConfig, setFormConfig] = useState({ saving: false, data: [], modalVisible: false })
    const [form] = Form.useForm();

    const redirectToHome = () => {
        actions.getAllForms()
        history.push('/')
    }

    const onFinish = (values) => {
        let data = { ...values, configuration: formConfig.data }
        setFormConfig(prevState => ({ ...prevState, saving: true }))
        makeRequest.post(CREATE_NEW_FORM, { data })
            .then(response => {
                if (response?.data?.status?.code == 200) {
                    Modal.success({
                        title: "New Form Created successfully",
                        content: <a href={`${window.location.origin}/form/${response.data.data.key}`} target="_blank">Click here to open</a>,
                        onOk: () => {
                            redirectToHome()
                        }
                    })
                }
                setFormConfig(prevState => ({ ...prevState, saving: false }))
            })
            .catch(err => {
                setFormConfig(prevState => ({ ...prevState, saving: false }))
                console.log(err)
            })
    };

    const addFormItem = (itemData) => {
        setFormConfig(prevState => ({ ...prevState, data: [...prevState.data, { ...itemData }], modalVisible: false }))
    }

    const setModalVisible = (value) => {
        setFormConfig(prevState => ({ ...prevState, modalVisible: value }))
    }

    return (
        <div className="create-form-container">
            <h3 className="title">Create New Form</h3>
            {
                formConfig?.modalVisible ?
                    <AddQuestionModal addFormItem={addFormItem} onCancel={() => setModalVisible(false)} />
                    : null
            }
            <Form
                layout="vertical"
                form={form}
                name="create-form"
                onFinish={onFinish}
            >
                <Form.Item label="Form name" name="name" rules={[{ required: true, message: "Please enter form name" }]}>
                    <Input placeholder="Enter name of form" />
                </Form.Item>
                <fieldset>
                    <legend>Form Preview :</legend>
                    <div className="form-details">
                        <FormComponent formData={{ configuration: formConfig.data }} />
                        <Button type="default" block onClick={() => setModalVisible(true)}> Add Question</Button>
                    </div>
                </fieldset>
                <Button loading={formConfig?.saving} type="primary" htmlType="submit" style={{ float: 'right' }} disabled={!formConfig?.data?.length}>Create Form</Button>
            </Form>
        </div>
    )
}

export default withRouter(reduxHOC(CreateFormComponent))
