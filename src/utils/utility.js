const moment = require('moment-timezone');

export const showDateTime = (timestamp) => {
    if (!isNaN(timestamp) && ((new Date()).getTime() - timestamp) < (2 * 24 * 60 * 60 * 1000)) {
        return moment.utc(timestamp).local().calendar()
    }
    else if (isNaN(timestamp) && ((new Date()).getTime() - (new Date(timestamp)).getTime()) < (2 * 24 * 60 * 60 * 1000)) {
        return moment.utc(timestamp).local().calendar()
    }
    else
        return moment.utc(timestamp).local().tz(moment.tz.guess()).format('lll z')
}