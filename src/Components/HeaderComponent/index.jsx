import React from 'react'
import { Menu } from 'antd'
import { withRouter } from 'react-router'

const HeaderComponent = ({ history, location }) => {
    return (
        <div className="header-main-container">
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[location?.pathname == '/create-new-form' ? "create-new-form" : "home"]}>
                <Menu.Item key={"home"} onClick={() => history.push('/')}>{`Home`}</Menu.Item>
                <Menu.Item key={"create-new-form"} onClick={() => history.push('/create-new-form')}>{`Create New Form`}</Menu.Item>
            </Menu>
        </div>
    )
}

export default withRouter(HeaderComponent)
