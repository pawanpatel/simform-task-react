import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import React from 'react'
import { getAllForms } from '../Redux/actions/formAction'

export default (Component) => {
    const InnerComponent = (props) => {
        return <Component {...props} />
    }

    const mapStateToProps = (state) => {
        return ({
            forms: state.forms
        })
    }

    const mapDispatchToProps = (dispatch) => {
        return ({
            actions: bindActionCreators({
                getAllForms,
            }, dispatch)
        })
    }

    return connect(mapStateToProps, mapDispatchToProps)(InnerComponent)

}
