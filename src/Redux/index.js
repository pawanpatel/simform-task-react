import { combineReducers, createStore, applyMiddleware } from 'redux'
import forms from './reducers/formReducer'
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    forms
})

export default createStore(rootReducer, applyMiddleware(thunk))
