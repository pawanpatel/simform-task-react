import React, {useState} from 'react'
import { Input, Table } from 'antd';
import reduxHOC from '../../hoc/ReduxHOC';
import { showDateTime } from '../../utils/utility';
// import ReduxHOC from '../../hoc/ReduxHOC';

const { Search } = Input;

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Created On',
        dataIndex: 'createdOn',
        key: 'createdOn',
        render: (text, record) => showDateTime(record.createdOn)
    },
    {
        title: 'Link',
        dataIndex: 'key',
        key: 'key',
        render: (text, record) => <a target="_blank" href={`${window.location.origin}/form/${record.key}`}>{`${window.location.origin}/form/${record.key}`}</a>
    },
];

const FormListComponent = ({ forms }) => {

    const [searchValue, setSearchValue] = useState("")

    const onSearch = (e) => {
        setSearchValue(e.target.value)
    }

    return (
        <div className="form-list-main-container">
            <div className="header">
                <h3>All forms</h3>
                <Search placeholder="Search by Name" onChange={onSearch} style={{ width: 200 }} />
            </div>
            <div className="list-container">
                <Table dataSource={forms?.filter(form => searchValue ?
                     form?.name?.toLowerCase().includes(searchValue?.toLowerCase()) 
                     : true) || []} columns={columns} pagination={false} />
            </div>
        </div>
    )
}

export default reduxHOC(FormListComponent)
