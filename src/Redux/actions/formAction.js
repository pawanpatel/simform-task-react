import { GET_ALL_FORMS } from "../../constants/urlConstants"
import makeRequest from "../../utils/makeRequest"


export const getAllForms = () => {
    return (dispatch) => {
        makeRequest.get(GET_ALL_FORMS)
            .then(response => {
                if (response?.data?.status?.code == 200) {
                    dispatch({
                        type: "SET_FORMS_DATA",
                        payLoad: response?.data?.data || []
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
    }
}